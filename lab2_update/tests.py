from simulator import *
import unittest
import sys


class MockInformer(Informer):

  def show_message(self, message):
    pass


class TestPlane(unittest.TestCase):

  def setUp(self):
    self.plane = Plane()

  def test_init_angle(self):
    plane_angle = self.plane.get_current_angle()
    self.assertEqual(90, plane_angle, 'Invalid initial plane angle')

  def test_turn_left(self):
    init_angle = self.plane.get_current_angle()
    self.plane.turn_left()
    plane_angle = self.plane.get_current_angle()
    self.assertEqual(
        init_angle - 1, plane_angle, 'Invalid angle after turn left')

  def test_turn_right(self):
    init_angle = self.plane.get_current_angle()
    self.plane.turn_right()
    plane_angle = self.plane.get_current_angle()
    self.assertEqual(
        init_angle + 1, plane_angle, 'Invalid angle after turn right')


class TestPilot(unittest.TestCase):

  def setUp(self):
    self.pilot = Pilot(MockInformer())

  def test_update(self):
    self.pilot.update(5)
    self.assertEqual(
        5, self.pilot.angle_to_correct, 'Invalid angle to correct value')


class TestIndicator(unittest.TestCase):

  def setUp(self):
    self.indicator = Indicator()

  def test_subscribe(self):
    self.indicator.subscribe(object())
    self.assertEqual(
        1, len(self.indicator.subscribers), 'Invalid subscribers count')


if __name__ == '__main__':
  unittest.main()

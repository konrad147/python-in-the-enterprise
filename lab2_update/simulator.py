# coding=utf-8

from random import *
import time


class Informer(object):

  def show_message(self, message):
    print message


class Indicator(object):

  def __init__(self):
    self.subscribers = []
    self.angle = 90

  def subscribe(self, observer):
    self.subscribers.append(observer)

  def notify(self, angle):
    for subscriber in self.subscribers:
      subscriber.update(angle)

  def check_values(self):
    side = randint(0, 6)
    angle_change = 0
    if side == 1:
      angle_change = randint(-5, 1)
    if side == 2:
      angle_change = randint(1, 5)
    self.angle = self.angle + angle_change
    self.notify(angle_change)


class Pilot(object):

  def __init__(self, informer):
    self.informer = informer
    self.angle_to_correct = 0

  def update(self, angle):
    if angle != 0:
      self.informer.show_message(
          "Wiatr przechylił samolo o: {0} stopni".format(angle))
      self.angle_to_correct = self.angle_to_correct + angle

  def watch_indicators(self, indicator):
    indicator.subscribe(self)

  def watch_angle(self, plane):
    if self.angle_to_correct > 0:
      self.angle_to_correct = self.angle_to_correct - 1
      plane.turn_left()
      informer.show_message(
          "Korekta samolotu. Obecny kat: {0}".format(plane.get_current_angle()))
    elif self.angle_to_correct < 0:
      self.angle_to_correct = self.angle_to_correct + 1
      plane.turn_right()
      informer.show_message(
          "Korekta samolotu. Obecny kat: {0}".format(plane.get_current_angle()))
    else:
      informer.show_message("Samolot leci prawidłowo")


class Plane(object):

  def __init__(self):
    self.indicator = Indicator()
    self.angle = 90

  def turn_right(self):
    self.indicator.angle += 1

  def turn_left(self):
    self.indicator.angle -= 1

  def get_current_angle(self):
    return self.indicator.angle


if __name__ == '__main__':
  informer = Informer()
  plane = Plane()
  pilot = Pilot(informer)
  pilot.watch_indicators(plane.indicator)
  print "Startujemy!!!"

  while True:
    plane.indicator.check_values()
    pilot.watch_angle(plane)
    time.sleep(0.2)

from simulator import *
import unittest
import sys

class TestPlane(unittest.TestCase):
	
	def setUp(self):
		self.plane = Plane()

	def test_correct_angle(self):
		correctAngle = 90
		plane.correct_angle(10)
		afterAngle = self.plane.angle
		self.assertEqual(correctAngle, afterAngle)


class TestIndicator(unittest.TestCase):

	def setUp(self):
		self.indicator = Indicator()

	def test_left_angle(self):
		leftAngle = self.indicator.get_angle(1)
		self.assertTrue(leftAngle < 0)

	def test_right_angle(self):
		rightAngle = self.indicator.get_angle(2)
		self.assertTrue(rightAngle > 0)

	def test_no_side(self):
		noAngle = self.indicator.get_angle(0)
		self.assertEqual(noAngle, 0)


class TestPilot(unittest.TestCase):

	def setUp(self):
		self.pilot = Pilot()
		self.plane = Plane()

	def test_correct_plane(self):
		self.pilot.correct_plane(self.plane, 5)
		self.assertEqual(self.plane.angle, 90)


if __name__ == '__main__':
	sys.stdout = open('/dev/null', 'w')
	unittest.main()



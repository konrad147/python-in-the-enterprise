#coding=utf-8

from random import randint
import time

class Indicator:
	def get_side(self):
		return randint(0,2)
	def get_angle(self, side):
		angle = 0;
		if side == 1:
			angle = randint(-10,0)
		if side == 2:
			angle = randint(0,10)
		return angle

class Plane:
	def __init__(self):
		self.speed = 100
		self.angle = 90
		self.indicator = Indicator();

	def correct_angle(self, angle):
		self.angle += angle;
		
		while self.angle != 90:
			if self.angle < 90:
				print "Korekta o 1 stopien"
				self.angle = self.angle + 1
			if self.angle > 90:
				print "Korekta o -1 stopien"
				self.angle = self.angle -1
			print "Obecny kat nachylenia: " + str(self.angle)
			time.sleep(0.5)
		print "\n"
	
class Pilot:
	def checkIndicators(self, plane):
		side = plane.indicator.get_side()
		angle = plane.indicator.get_angle(side)

		if side == 0:
			print "Samolot nie przechylony";
		if side == 1:
			print "Samolot przechylony w prawo o " + str(angle) + " stopni" 
		if side == 2:
			print "Samolot przechylony w lewo o " + str(angle) + " stopni"
		print "\n"
		return angle
	def correct_plane(self, plane, angle):
		plane.correct_angle(angle)	

plane = Plane()
pilot = Pilot()
#indicator = Indicator()

while True:
	angle =	pilot.checkIndicators(plane)
	if angle != 0:
		pilot.correct_plane(plane, angle)

	time.sleep(0.5)


